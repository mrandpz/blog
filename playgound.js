// try playground

function _new() {
  // 获取到构造函数。例：Foo
  const Constructor = Array.prototype.shift.call(arguments);
  // 创建一个空对象
  const obj = {};
  // 将该对象的原型链指向构造函数的原型
  obj.__proto__ = Constructor.prototype;
  // 绑定 obj 到构造函数
  const ret = Constructor.apply(obj, arguments);
  // 判断当前返回值是否为对象，是的话直接返回对象，否则返回新创建的obj
  return typeof ret === 'object' ? ret : obj;
}

function Foo() {
  console.log(this); // Foo
}

const a = _new(Foo);
