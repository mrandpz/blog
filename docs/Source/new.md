---
new 运算符源代码
---

new 操作符做了什么 `var o = new Foo()`

1.  获取到当前的构造函数 Constructor
2.  创建一个空对象 const obj = {}
3.  将当前空对象的原型链指向当前构造函数的原型
4.  利用 apply，将 Constructor 的 this 绑定到 obj，并运行得出运行结果
5.  如果返回的是一个对象，则直接返回这个对象，否则返回 obj

```js
function _new() {
  // 获取到构造函数。例：Foo
  const Constructor = Array.prototype.shift.call(arguments);
  // 创建一个空对象
  const obj = {};
  // 将该对象的原型链指向构造函数的原型
  obj.__proto__ = Constructor.prototype;
  // 绑定 obj 到构造函数
  const ret = Constructor.apply(obj, arguments);
  // 判断当前返回值是否为对象，是的话直接返回对象，否则返回新创建的obj
  return typeof ret === 'object' ? ret : obj;
}
```

验证下结果

```js
function Foo() {
  console.log(this); // Foo
}

const a = _new(Foo);
```
