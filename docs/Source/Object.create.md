---
Object.create
---

```js
Object.create = function(proto, propertiesObject) {
  function F() {}
  F.prototype = proto; // 所以 当前被创建出来的字对象 son.prototype === proto 传进来的对象

  return new F();
};
```
