---
title: docker
---

# 简介

本篇文章没有涉及 docker-compose 和镜像上传，前端够用

`docker`的 logo 看起来就是一个鲸鱼，运着集装箱，这些集装箱装着标准的货物（`Nginx，jenkins`），他们之间互不影响，不需要考虑是在`unbuntu`还是`centos`
比如我安装`jenkins`的时候需要先考虑安装`java`环境，安装 java 环境的时候需要去考虑资源是否过期，但是有了`docker`之后，想要安装`jenkins`直接执行`docker pull jenkins`,一个现成的[jenkins](/tool-chain/jenkins)就可以使用了

## docker 的安装

[安装教程](https://www.runoob.com/docker/centos-docker-install.html)

### 启动，重启，关闭

我的服务器是 Centos

```
sudo systemctl start docker
systemctl restart  docker
docker systemctl stop docker
```

## docker 的命令和参数

拉取镜像

```bash
docker pull jenkins
docker pull nginx
```

## 镜像和容器的区别

镜像指的是`jenkins`，`Nginx`，而容器表示的是以下命令启动的容器

```
docker run -d -p 8888:8080 -p 50000:50000 -v jenkins:/var/jenkins_home -v /etc/localtime:/etc/localtime --name jenkins docker.io/jenkins/jenkins
docker exec -u 0 -it b1dad0fc3a82 /bin/bash 进入容器
docker ps -a    查看当前运行的容器
docker images   当前下载的景象
docker rm b1dad0fc3a82(或者名字) 删除容器
docker rmi 删除镜像
```

- -d 让容器在后台运行
- -p 端口映射
- -v 文件夹映射
- --name 启动容器的名字
- -u 指定用户名
- -i: 交互式操作。
- -t: 终端。
