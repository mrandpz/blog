---
title: jenkins
---

## 简介

[jenkins](https://www.jenkins.io/zh/doc/pipeline/tour/getting-started/) 是一个开源的自动化部署工具，一开始我的代码是通过本地或者服务器`npm run build`，手动执行打包，这个步骤，就要去连接服务器，本地的话还要`ftp`传输文件。是一个体验上不是很舒服的操作。所以就看到了`jenkins`这个工具,以下是前端的基础应用，更具体的还请查阅[官方文档](https://www.jenkins.io/zh/doc/pipeline/tour/getting-started/)

## 运行 jenkins

首先第一篇我简单介绍了[docker](/tool-chain/docker),并且执行

```bash
docker pull jenkins
docker run -d -p 8080:8080 -p 50000:50000 -v jenkins:/var/jenkins_home -v /etc/localtime:/etc/localtime --name jenkins docker.io/jenkins/jenkins
```

### 打开网页之前

jenkins 插件安装的时候会有超时，因为都是请求的 google.com，所以要先替换下

```
cd /var/lib/jenkins/updates/
sed -i 's/http:\/\/www.google.com/https:\/\/www.baidu.com/g' default.json
sed -i 's/http:\/\/updates.jenkins-ci.org\/download/https:\/\/mirrors.tuna.tsinghua.edu.cn\/jenkins/g' default.json
```

### 进入容器

```
docker exec -u 0 -it 容器名称或者id /bin/bash
```

此时我们就启动了一个容器，<b>阿里云打不开的话查看下防火墙，和安全组是否开放端口</b>,在浏览器打开 http://babaloveyou.com:8080/ 可以看到`jenkins`启动了，此时会要求我们输入一个密码

```bash
cat /var/jenkins_home/secrets/initialAdminassword 拿到密码
```

输入密码，点击默认插件安装，设置下管理员的账号密码就可以开始使用了

## 安装 Publish over SSH

在 http://xxx:8080/configure 页面配置，这里也可以配置邮箱等等
![image](./images/Publish.png)

- Passphrase 填写服务器的密码
- Hostname 服务器地址
- Username 登录名字
- Remote Directory 这个是到时候 send file 的时候的 base 目录，注意了，这里是 base 目录

## 新建一个自由风格的软件项目

1、点击新建任务

2、源码管理中选中 git，github 的地址也行
![image](./images/git_addr.png)
3、配置传输文件
![image](./images/send.png)
4、因为我的服务器是屌丝机，又要放很多项目，所以我直接打包推送到 gitlab，如果想在服务器打包可以在 exec command 中输入命令

此时一个很简单的自动发布就完成了

## 流水线风格

1、点击新建任务，选择一个流水线风格

2、拉倒最底下，进入 流水线语法

![image](./images/pipeline.png)

3、安装 sshPublisher
![image](./images/sspublish.png)

```bash
node {
  checkout([$class: 'GitSCM', branches: [[name: 'xxx']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: true]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'yefenggitlab', url: 'http://xxx.git']]])

   stage('build'){
    //   sh 'npm install'
    //   sh 'npm run build'
   }

   stage('tar'){
        dir('dist'){
          sh 'tar cvf dist.tar.gz *'
          sh 'ls'
        }
   }

   stage('publish'){
       sshPublisher(publishers: [sshPublisherDesc(configName: 'dev', transfers: [sshTransfer(cleanRemote: false, excludes: '', execCommand: '''ls
rm -rf /xxx/xxx
tar xvf dist/dist.tar.gz -C /xxx/xxx''', execTimeout: 120000, flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectory: '', remoteDirectorySDF: false, removePrefix: '', sourceFiles: 'dist/dist.tar.gz')], usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: false)])
   }
}
```

完成。

jenkins 还可以根据 git hook 来决定要不要编译，发布打包，但是我觉得这个东西还是手动来安全一些
