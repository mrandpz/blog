> 经常重复的执行 git add，git commit。所以招了个 bash 文件代替快捷式提交代码

```bash
#!/bin/bash
git status

read -r -p "是否继续提交? [Y/n] " input

case $input in
    [yY][eE][sS]|[yY])
        echo "继续提交"
        git add .
        git commit -m $1
        git push origin $2
                    exit 1
        ;;

    [nN][oO]|[nN])
        echo "中断提交"
        exit 1
            ;;

    *)
    echo "输入错误，请重新输入"
    ;;
esac
```

执行 `./gitbash.sh 添加自动化 master` => `./gitbash.sh commitMessage branch`
