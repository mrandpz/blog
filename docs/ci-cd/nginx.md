---
title: nginx
---

## 安装

1. docker pull nginx
2. 查看镜像 docker images nginx
3. 启动一个

```bash
docker run -d -p 8082:80
-p 443:443  配置https需要用到
--name nginx-web
-v ~/nginx/www:/usr/share/nginx/html
-v ~/nginx/conf/nginx.conf:/etc/nginx/nginx.conf
-v ~/nginx/logs:/var/log/nginx nginx
```

```bash
-v ~/nginx/www:/usr/share/nginx/html
-v ~/nginx/conf/nginx.conf:/etc/nginx/nginx.conf
-v ~/nginx/logs:/var/log/nginx
 表示把服务器上的目录映射到容器内的目录。在此之前我们可以先拷贝一份出来，然后再修改
```

拷贝容器内 Nginx 默认配置文件到本地当前目录下的 conf 目录，容器 ID 可以查看 docker ps 命令输入中的第一列：

```bash
docker cp 6dd4380ba708:/etc/nginx/nginx.conf ~/nginx/conf
```

## 配置 nginx server

一般前端不怎么需要配置 Nginx，但是如果想要发布单页面应用，配置 https，或者实现浏览器不缓存 index.html 等静态资源策略，就需要配置 Nginx，当然静态资源策略还需要配置浏览器的 meta 标签

```bash
server {
    listen       80;
    server_name  xxx.xxx.com;

    location / {
        root   html/xxx/xx;  //
        index  index.html index.htm;
        try_files $uri $uri/ index.html;  // 配置单页面时，需要这一行重定向，否则刷新回404

    }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   html;
    }

}
```

## 开启 gzip

部署成功，打开能正常访问之后，发现我们的浏览器浏览器的文件跟我们发布时候一样。这时候可以配置下 gzip 提升下速度

加 gzip 之前
![image](./images/gzip-before.png)

之后   
![image](./images/after.png)

```bash
    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;
# $gzip_ratio计算请求的压缩率，$body_bytes_sent请求体大小
    # log_format  main  '$remote_addr - $remote_user [$time_local] "$host" - "$request" '
    #                 '$gzip_ratio - $body_bytes_sent - $request_time';


    # access_log  logs/access.log  main;

    # 开启gzip
    gzip on;

    # 启用gzip压缩的最小文件，小于设置值的文件将不会压缩
    gzip_min_length 1k;

    # gzip 压缩级别，1-9，数字越大压缩的越好，也越占用CPU时间，后面会有详细说明
    gzip_comp_level 1;

    # 进行压缩的文件类型。javascript有多种形式。其中的值可以在 mime.types 文件中找到。
    gzip_types text/plain application/javascript application/x-javascript text/css application/xml text/javascript application/x-httpd-php image/jpeg image/gif image/png application/vnd.ms-fontobject font/ttf font/opentype font/x-woff image/svg+xml;

    # 是否在http header中添加Vary: Accept-Encoding，建议开启
    gzip_vary on;

    # 禁用IE 6 gzip
    gzip_disable "MSIE [1-6]\.";

    # 设置压缩所需要的缓冲区大小
    gzip_buffers 32 4k;

    # 设置gzip压缩针对的HTTP协议版本
    gzip_http_version 1.0;
```

## 配置 https

有时候我们会发现页面上出现了一些莫名其妙的广告，那是 http 导致了运营商劫持，所以我们可以配置 https 解决这个问题，而且顺带解决了被抓包的问题

<!-- todo ，更具体的访问 http https http2 的区别  -->

### 申请证书

生成私钥文件：

```
openssl genrsa -des3 -out server.key 2048
```

去除口令：

```
mv server.key server.key.back
openssl rsa -in server.key.back -out server.key
```

创建请求证书：

```
openssl req -new -key server.key -out server.csr
```

生成证书文件：

```
openssl x509 -req -days 36500 -in server.csr -signkey server.key -out server.crt
```

修改 nginx 配置
增加 rewrite 配置

```
  server {
        listen       80;
        server_name  localhost;

        location / {
            root   html/entry/dist;
            index  index.html index.htm;
            try_files $uri $uri/ index.html;
        }
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
        rewrite ^(.*)$ https://$host$1 permanent;
    }
```

增加 443 端口配置,端口配置的时候记得打开安全组规则的端口

```
 server {
		listen  443 ssl;
        server_name  localhost;

		ssl		on;
		ssl_certificate      xxx/server.crt;
		ssl_certificate_key  xxx/sbin/server.key;

		# 协议优化(可选,优化https协议,增强安全性)
		ssl_prefer_server_ciphers  on;
		ssl_session_cache    shared:SSL:10m;
		ssl_session_timeout  10m;
		ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;
		ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

         location / {
            root   html/entry/dist;
            index  index.html index.htm;
            try_files $uri $uri/ index.html;
        }
    }
```


## TODO： 配置 http2