---
title: 私有npm
---

> 公司想要提高自内部的办事效率，复用已写好的组件，又不想把代码放到公网，这时候可以在内部搭建一个私有 npm

## 示例

http://babaloveyou.com:4873/
http://babaloveyou.com

## 用 `verdaccio` 搭建一个私有仓库

verdaccio： 0 安装 0 配置的仓库

1. npm i -g verdaccio
2. npm adduser --registry http://babaloveyou.com:4873/
3. 输入账号，密码，邮箱

## 用 `dumi`写公司内部的文档，并且搭建文档服务器

[`dumi`](https://d.umijs.org/) 提供编写组件，一键式发布 `npm` 包，并且提供了可视化的组件的功能。

注意点：

1. `npm`包的 name 应该用 scope
2. 限定发布的文件，文件夹，在`package.json` 中增加`files`字段
3. 执行 `npm publish`
