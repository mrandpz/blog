---
title: Mr.pz - 前端路漫漫
order: 10
hero:
  title: 防止老年痴呆
  desc: 开个文档来记录自己的工作，学习的过程
  actions:
    - text: Ready Go
      link: /base
features:
  # - icon: https://gw.alipayobjects.com/zos/bmw-prod/881dc458-f20b-407b-947a-95104b5ec82b/k79dm8ih_w144_h144.png
  #   title: 重学前端
  #   desc: 整理前端知识点
  # - icon: https://gw.alipayobjects.com/zos/bmw-prod/d60657df-0822-4631-9d7c-e7a869c2f21c/k79dmz3q_w126_h126.png
  #   title: 算法
  #   desc: 防衰老
  # - icon: https://gw.alipayobjects.com/zos/bmw-prod/d1ee0c6f-5aed-4a45-a507-339a4bfe076c/k7bjsocq_w144_h144.png
  #   title: 源码学习
  #   desc: 工作上遇到问题能够更快的定位
  # - icon: https://gw.alipayobjects.com/zos/bmw-prod/d1ee0c6f-5aed-4a45-a507-339a4bfe076c/k7bjsocq_w144_h144.png
  #   title: electron + vue
  #   desc: 锻炼nodejs的能力
footer: 记忆中的小脚丫 肉嘟嘟的小嘴巴 一生把爱交给他
---
