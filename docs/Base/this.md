---
title: this 指向
---

> this 指向是在<mark>函数真正被调用的时候确定的。</mark>我一直记着这句话，在我工作中也给我了莫大的帮助。直到有一天我被问到：箭头函数的`this`指向，箭头函数`bind(a).bind(b)`之后的 this 指向，我发现，我的理解还是不够，也希望大家能够给出更多的案例，一起去学习这个 this 指向

tips: 看代码的同时，记得右上角 copy 代码，自己运行一遍哟，有问题也可以及时交流

## 构造函数内的 this 指向

### 当做普通函数来调用

```js
const name = 'outer';
function Foo() {
  console.log(this);
}
Foo();
// 因为const声明，默认是严格模式，此时的this.name是 undefined，指向window。 如果变量是用var声明，非严格模式指向window，this.name是 outer
```

### 当做构造函数来调用

```js
function Foo() {
  this.a = 88;
}
const o = new Foo();
console.log(o.a); // 88
// this
```

### 当构造函数里返回了 object

```js
function Foo() {
  this.a = 88;
  return {
    a: 66,
  };
}
console.log(Foo()); // { a: 66 }

var o = new Foo();

console.log(o); // { a: 66 }
```

查看 [new 源码](/source/new)，知道为什么 return {} 后，即使 new 了一个元素，还是指向 return 的对象

### 当构造函数里面有宏任务

```js
function Foo() {
  this.a = 88;
  setTimeout(() => {
    console.log(this); // this指向容器 Foo，普通函数调用还是window
  });
}

var o = new Foo();
```

## 对象内的方法

```js
var o = {
  prop: 37,
  f: function() {
    return this.prop;
  },
};

console.log(o.f()); // 37
```

如果先把 f 拿出来，那就是相当于在全局调用了这个方法

```js
var prop = 36;
var o = {
  prop: 37,
  f: function() {
    return this.prop;
  },
};

var windowF = o.f;
console.log(windowF()); // undefined,非严格模式下36
```

## 原型链中的 this

```js
var o = {
  f: function() {
    return this.a + this.b;
  },
};
var p = Object.create(o);
console.log(p);
p.a = 1;
p.b = 4;

console.log(p.f()); // 5
// f是作为p的方法调用的，所以它的this指向了p
```

## 箭头函数的 this 指向

箭头函数没有自己的 this，<mark>箭头函数的 this 指向，根据他的容器确定 this 指向</mark>

```js
var obj = {
  bar: function() {
    var x = () => this;
    return x;
  },
};

var fn = obj.bar();
console.log(fn() === obj); // true
// 此时，也就是 obj。

// 如果是先获取到bar
var fn2 = obj.bar;
// 再进行调用
console.log(fn2()() == window); // true
// 此时他的容器 obj的指向window，所以箭头函数也是指向window
```

## call，apply，bind 改变 this

### apply,call

```js
// 将一个对象作为call和apply的第一个参数，this会被绑定到这个对象。
var obj = { a: 'Custom' };

// 这个属性是在global对象定义的。
var a = 'Global';

function whatsThis(arg) {
  return this.a; // this的值取决于函数的调用方式
}

whatsThis(); // 'Global'
whatsThis.call(obj); // 'Custom'
whatsThis.apply(obj); // 'Custom'
```

### bind

```js
function f() {
  return this.a;
}

var g = f.bind({ a: 'azerty' });
console.log(g()); // azerty

var h = g.bind({ a: 'yoo' }); // bind只生效一次！
console.log(h()); // azerty

var o = { a: 37, f: f, g: g, h: h };
console.log(o.a, o.f(), o.g(), o.h()); // 37, 37, azerty, azerty
```

### 箭头函数改变 this

箭头函数是没有自己的 this 指向的，所以无法改变 this，第一个参数可以传`null`

[查看源码，知道为什么 apply，call，bind 能修改 this 指向,bind 只能生效一次](/source/change-this)

## 参考资料

[深入理解 javascript 原型和闭包（10）——this](https://www.cnblogs.com/wangfupeng1988/p/3988422.html)

[MDN this 指向](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/this#%E6%9E%84%E9%80%A0%E5%87%BD%E6%95%B0%E4%B8%AD%E7%9A%84_this)

[MDN 箭头函数](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/arrow_functions#%E5%83%8F%E6%96%B9%E6%B3%95%E4%B8%80%E6%A0%B7%E4%BD%BF%E7%94%A8%E7%AE%AD%E5%A4%B4%E5%87%BD%E6%95%B0)
