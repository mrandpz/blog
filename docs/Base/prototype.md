---
title: 原型，原型链
---

> 很多人估计看到代码能看的懂原型，原型链，却没办法有效的组织语言。哈哈哈，当然，很多人指的是我。

# 本文重点

每个对象都有一个 `__proto__`属性，指向创建该对象的函数的`prototype`。

## 这说的都是什么

1. `JavaScript`常被描述为一种基于原型的语言
2. 每个对象有一个原型对象
3. 对象以其原型为模板，从原型继承方法和属性。
4. 原型对象也可能有原型，并继承方法和属性，一层一层，以此类推，这种关系常被称为 `原型链`

以上 MDN 也说了，很抽象，我也觉得很抽象，背不下来，背不下来。不过先看一遍上面的语句有个印象。

## 先有爸爸

```js
function father() {}
father.prototype.foo = 'fatherFoo';
console.log(father.prototype);
```

在控制台会有以下显示

```js
{
    // 每个实例对象都从原型中继承了一个 constructor 当前为 ƒ father(),
    constructor: ƒ father(),
    // 此为后期自己添加的原型对象
    foo: "fatherFoo",
    // 每个对象都有一个__proto__属性，指向创建该对象的函数的 prototype。
    // 当前实例往上找的实例对象 father.prototype.__proto__  === Object.prototype
    __proto__: {
        constructor: ƒ Object(),
        hasOwnProperty: ƒ hasOwnProperty(),
        isPrototypeOf: ƒ isPrototypeOf(),
        propertyIsEnumerable: ƒ propertyIsEnumerable(),
        toLocaleString: ƒ toLocaleString(),
        toString: ƒ toString(),
        valueOf: ƒ valueOf()
    }
}
```

<mark>tips: 每个对象都有一个**proto**属性，指向创建该对象的函数的 prototype。</mark>  
作为一个`father` ，可以去用祖辈的任意东西，比如`father`中调用 hasOwnProperty 会从`object` 中拿

## 造个儿子

```js
const son = Object.create(father.prototype);
console.log(son.prototype);

son.__proto__ === father.prototype; // 每个对象都有一个__proto__属性，指向创建该对象的函数的 prototype。
```

Object.create 的大概代码

```js
Object.create = function(proto, propertiesObject) {
  function F() {}
  F.prototype = proto; // 所以 当前被创建出来的字对象 son.prototype === father 传进来的对象
  return new F();
};
```

## 试试从 son 往上找

```js
son.__proto__ === father.prototype;
father.prototype.__proto__ === Object.prototype;
Object.prototype.__proto__ === null;
```

emmm，原型链的顶端是`null` 有点乱，找了个好图。  
![images](./images/prototype.png)

基本上这个图都好理解，就是有一个环不好理解，可以稍微理一下，可以先自己在控制台打印下 `Function`、`Object` 发现都是一个 `function`

每个对象都有一个**proto**属性，指向创建该对象的函数的 prototype。

```js
Function.__proto__ === Function.prototype; // => 跟之前创建的 father 对比下 father.__proto__ !== father.prototype
Object.__proto__ === Function.prototype; //  每个对象都有一个__proto__属性，指向创建该对象的函数的 prototype。
Function.prototype.__proto__ === Object.prototype;
```

可以看出

```js
Object instanceof Function; // true
Function instanceof Function;
Function instanceof Object; // ==> Function.prototype.__proto__ === Object.prototype
```

## 后记

估计看完还是会有点乱，可以多参照这张图，多在控制台打印，就能慢慢理解了

如果有人再问我原型是什么，我大概会把 <a href="#这说的都是什么">这说的是都是什么说一遍吧</a>

## 引用

[MDN 对象原型](https://developer.mozilla.org/zh-CN/docs/Learn/JavaScript/Objects/Object_prototypes)

[深入理解 javascript 原型和闭包（5）——instanceof](https://www.cnblogs.com/wangfupeng1988/p/3979533.html)
