import { defineConfig } from 'dumi';

// more config: https://d.umijs.org/config
export default defineConfig({
  favicon: '/favicon.ico',
  logo: '/logo.png',
  mode: 'site',
  hash: true,
  title: 'showhand',
  navs: [
    null,
    { title: 'npm', path: 'http://babaloveyou.com:4873/' },
    { title: 'GitHub', path: 'https://github.com/mrandpz' },
  ],
  extraBabelPlugins: [
    [
      'import',
      {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: 'css',
      },
    ],
  ],
  exportStatic: {},
  outputPath: 'docs-dist',
});
